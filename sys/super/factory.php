<?php
namespace sys\super;

interface factory
{
	public static function getInstance($name=null);
}
